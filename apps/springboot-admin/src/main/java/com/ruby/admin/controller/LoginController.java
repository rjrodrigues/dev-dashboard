package com.ruby.admin.controller;

import com.ruby.admin.dto.LoginRequestDto;
import com.ruby.admin.dto.LoginResultDto;
import com.ruby.admin.service.LoginService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/login")
public class LoginController {

  private final LoginService loginService;

  public LoginController(LoginService loginService) {
    this.loginService = loginService;
  }

  @PostMapping
  LoginResultDto login(LoginRequestDto loginRequestDto) {
    return new LoginResultDto(this.loginService.login(loginRequestDto.getUsername(), loginRequestDto.getPassword()));
  }

}
